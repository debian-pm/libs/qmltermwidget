Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: qmltermwidget
Source: https://github.com/Swordfish90/qmltermwidget

Files: debian/*
Copyright: 2018, Jonah Brüchert <jbb@kaidan.im>
License: GPL-2.0+

Files: lib/*
Copyright: 2007, 2008, Robert Knight <robertknight@gmail.com>
License: GPL-2+

Files: lib/BlockArray.cpp
  lib/BlockArray.h
Copyright: 2008
  2000, Stephan Kulow <coolo@kde.org>
License: GPL-2+

Files: lib/Character.h
  lib/CharacterColor.h
  lib/Emulation.h
  lib/Pty.h
  lib/Screen.h
  lib/TerminalDisplay.cpp
  lib/TerminalDisplay.h
  lib/Vt102Emulation.h
Copyright: 2006-2008, Robert Knight <robertknight@gmail.com>
  1997, 1998, Lars Doelle <lars.doelle@on-line.de>
License: GPL-2+

Files: lib/Emulation.cpp
Copyright: 2007, 2008, Robert Knight <robertknight@gmail.com>
  1997, 1998, Lars Doelle <lars.doelle@on-line.de>
  1996, Matthias Ettrich <ettrich@kde.org>
License: GPL-2+

Files: lib/History.cpp
  lib/History.h
  lib/Pty.cpp
Copyright: 1997, 1998, Lars Doelle <lars.doelle@on-line.de>
License: GPL-2+

Files: lib/HistorySearch.cpp
  lib/HistorySearch.h
  lib/SearchBar.cpp
  lib/SearchBar.h
Copyright: 2013, Christian Surlykke
License: GPL-2+

Files: lib/ProcessInfo.cpp
Copyright: 2007, 2008, Robert Knight <robertknight@gmail.countm>
License: GPL-2+

Files: lib/Screen.cpp
  lib/Vt102Emulation.cpp
Copyright: 2007, 2008, Robert Knight <robert.knight@gmail.com>
  1997, 1998, Lars Doelle <lars.doelle@on-line.de>
License: GPL-2+

Files: lib/Session.cpp
  lib/Session.h
Copyright: 2008,
  2006, 2007, Robert Knight <robertknight@gmail.com>
  1997, 1998, Lars Doelle <lars.doelle@on-line.de>
License: GPL-2+

Files: lib/ShellCommand.cpp
  lib/ShellCommand.h
Copyright: 2008
  2007, Robert Knight <robertknight@gmail.com>
License: GPL-2+

Files: lib/TerminalCharacterDecoder.cpp
  lib/TerminalCharacterDecoder.h
Copyright: 2006-2008, Robert Knight <robertknight@gmail.com>
License: LGPL-2+

Files: lib/kprocess.cpp
  lib/kprocess.h
  lib/kptydevice.h
  lib/kptyprocess.cpp
  lib/kptyprocess.h
Copyright: 2007, Oswald Buddenhagen <ossi@kde.org>
License: LGPL-2+

Files: lib/kpty.cpp
Copyright: 2008
  2002, Waldo Bastian <bastian@kde.org>
  2002, 2003, 2007, Oswald Buddenhagen <ossi@kde.org>
License: LGPL-2+

Files: lib/kpty.h
  lib/kpty_p.h
Copyright: 2008
  2003, 2007, Oswald Buddenhagen <ossi@kde.org>
License: LGPL-2+

Files: lib/kptydevice.cpp
Copyright: 2010, KDE e.V. <kde-ev-board@kde.org>
  2007, Oswald Buddenhagen <ossi@kde.org>
License: LGPL-2+

Files: lib/qtermwidget.cpp
  lib/qtermwidget.h
Copyright: 2008, e_k (e_k@users.sourceforge.net)
License: LGPL-2+

Files: src/*
Copyright: 2013, Dmitry Zagnoyko <hiroshidi@gmail.com>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991, or (at
 your option) any later version.
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Library General Public License as published by the
 Free Software Foundation; version 2 of the License, or (at
 your option) any later version.
 On Debian systems, the complete text of version 2 of the GNU Library
 General Public License can be found in '/usr/share/common-licenses/LGPL-2'.
